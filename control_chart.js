google.charts.load('current', {packages: ['corechart', 'controls']});
google.charts.setOnLoadCallback(function() {
	drawDashboard();
});

function drawDashboard() {
	// Create our data table.
    var data = google.visualization.arrayToDataTable([
      ['Name', 'Pedaços de pizza comidos'],
      ['Michael' , 5],
      ['Elisa', 7],
      ['Robert', 3],
      ['John', 2],
      ['Jessica', 6],
      ['Aaron', 1],
      ['Margareth', 8]
    ]);

    // Create a dashboard.
    var dashboard = new google.visualization.Dashboard(
        document.getElementById('dashboard_div'));

        // Create a range slider, passing some options
        var donutRangeSlider = new google.visualization.ControlWrapper({
          'controlType': 'NumberRangeFilter',
          'containerId': 'filter_div',
          'options': {
            'filterColumnLabel': 'Pedaços de pizza comidos'
      	}
    });

    // Create a pie chart, passing some options
    var pieChart = new google.visualization.ChartWrapper({
      'chartType': 'PieChart',
      'containerId': 'chart_div',
      'options': {
        'width': 500,
        'height': 500,
        'pieSliceText': 'value',
        'legend': 'right'
      }
    });

    // Establish dependencies, declaring that 'filter' drives 'pieChart',
    // so that the pie chart will only display entries that are let through
    // given the chosen slider range.
    dashboard.bind(donutRangeSlider, pieChart);

    // Draw the dashboard.
    dashboard.draw(data);
}